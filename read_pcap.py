from utils.data_parser import DataParser
from utils.logger import Logger


logger = Logger("logs", "pcap_read")
dp = DataParser(logger)
dp.read_pcap_file("pcap_samples/fstream.binance.com_capture.pcap")