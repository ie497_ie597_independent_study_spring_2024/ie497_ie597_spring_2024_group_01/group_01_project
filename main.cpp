//
// Created by Zhicheng Tang on 3/30/24.
//
#include "src/utils/data_parser.h"
#include "src/utils/logger.h"
#include "src/websocket_client/binance_client.h"

int main(const int argc, const char *argv[]) {
    if (argc > 1) {
        string config_path = "job_configs.json";
        string job = argv[1];
        const shared_ptr<Logger> logger;
        auto data_parser = DataParser(logger);
        nlohmann::json job_configs = data_parser.getJobConfig(config_path, job);
        std::cout << job_configs.dump(4) << std::endl;

        if (job == "demo") {
            logger->log(INFO, "Start demo job.");
            auto client = BinanceClient(logger, false);
            client.run(job_configs["streams"], job_configs["margin_type"]);
        } else if (job == "test_multi_streams") {
            logger->log(INFO, "Start test multiple streams job.");
            auto client = BinanceClient(logger, false);
            client.run(job_configs["streams"], job_configs["margin_type"]);
        } else if (job == "read_binary") {
            logger->log(INFO, "Start read binary job.");
            vector<int> precisions = data_parser.getPrecisions("precision_configs.json",
                                                               job_configs["symbol"]);
            tuple<uint64_t, uint64_t, std::vector<Order>, std::vector<Order> > record = data_parser.readBinaryRecord(
                job_configs["read_path"], precisions[0], precisions[1]);
            cout << "Finish reading a binary record." << endl;
        } else if (job == "write_binary") {
            logger->log(INFO, "Start write binary job.");
            const chrono::seconds stop_time(job_configs["stop_time"]);
            auto client = BinanceClient(
                logger,
                job_configs["write_data"],
                job_configs["write_binary"],
                stop_time,
                job_configs["write_path"],
                job_configs["file_name"],
                job_configs["keep_original"]);

            const std::vector<std::string> streams = job_configs["streams"].get<vector<string> >();
            client.run(streams, job_configs["margin_type"]);
        } else {
            logger->log(INFO, "Job: " + job + " is not found.");
        }
    } else {
        cout << "No job input." << endl;
    }
}
