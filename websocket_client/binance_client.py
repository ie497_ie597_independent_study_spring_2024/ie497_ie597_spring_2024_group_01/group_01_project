import websocket
import socket
import json
import time
from multiprocessing import Queue
from utils.logger import Logger
from utils.data_parser import DataParser


class BinanceClient:
    def __init__(self, logger: Logger, message_queue: Queue, write_data: bool, write_binary=False, stop_time=None,
                 write_path=None, file_name=None, save_original_json=False) -> None:
        self.spot_base_url = "wss://stream.binance.com:9443"
        self.um_base_url = "wss://fstream.binance.com"
        self.logger = logger
        self.write_data = write_data
        self.write_binary = write_binary
        if write_data:
            self.write_path = write_path
            self.file_name = file_name
            self.message_queue = message_queue
        self.stop_time = stop_time
        self.data_parser = DataParser(logger=logger)
        self.logger.info("Client started.")
    def handle_message(self, ws, message: str) -> None:
        if hasattr(self, "message_queue"):
            self.message_queue.put(message)
            self.logger.info("Added a message to the queue.")
        elif self.write_binary:
            binary = self.data_parser.json_to_binary(message)
            self.data_parser.write_binary(binary, self.write_path, self.file_name)
        else:
            self.data_parser.write_json(message, self.write_path)

        if self.stop_time is not None and (time.time() - self.start_time > self.stop_time):
            if hasattr(self, "message_queue"):
                self.message_queue.put("stop")
            ws.close()

    def demo_message(self, ws, message: str) -> None:
        data = json.loads(message)
        print(data)
        if self.stop_time is not None and (time.time() - self.start_time > self.stop_time):
            ws.close()

    def on_error(self, error) -> None:
        self.logger.error(error)

    def on_close(self) -> None:
        self.logger.info("Connection closed.")

    def on_open(self, ws, streams: list[str]) -> None:
        ip_address = ws.sock.sock.getpeername()[0]
        try:
            host = socket.gethostbyaddr(ip_address)[0]
        except socket.herror:
            host = ip_address
        self.logger.info(f"Connection Opened. Streams: {streams}, Host: {host}, IP Address: {ip_address}")

    def on_ping(self):
        pass

    def on_pong(self):
        pass

    def get_ws_url(self, streams: list[str], margin_type=None):
        """
        Raw streams: /ws/<streamName>
        Combined streams: /stream?streams=<streamName1>/<streamName2>/<streamName3>
        """
        if margin_type is None:
            base_url = self.spot_base_url
        else:
            base_url = self.um_base_url

        if len(streams) == 1:
            ws_url = base_url + '/ws/' + streams[0]
        else:
            ws_url = base_url + f"/stream?streams={'/'.join(streams)}"
        return ws_url

    def run(self, streams: list[str], margin_type: str | None) -> None:
        ws_url = self.get_ws_url(streams, margin_type)
        if self.write_data:
            ws = websocket.WebSocketApp(ws_url,
                                        on_open=lambda ws: self.on_open(ws, streams),
                                        on_message=lambda ws, msg: self.handle_message(ws, msg),
                                        on_error=lambda ws, msg: self.on_error(msg),
                                        on_close=lambda ws: self.on_close()
                                        )
        else:
            ws = websocket.WebSocketApp(ws_url,
                                        on_open=lambda ws: self.on_open(ws, streams),
                                        on_message=lambda ws, msg: self.demo_message(ws, msg),
                                        on_error=lambda ws, msg: self.on_error(msg),
                                        on_close=lambda ws: self.on_close()
                                        )
        if self.stop_time is not None:
            self.start_time = time.time()
            self.logger.info(f"Run ws client for {self.stop_time} seconds.")
        ws.run_forever()
