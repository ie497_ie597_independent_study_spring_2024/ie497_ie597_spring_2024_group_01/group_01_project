import unittest
import json
from utils.logger import Logger
from utils.data_parser import DataParser


class TestDataCorrectness(unittest.TestCase):
    test_job_config = None

    def setUp(self):
        self.logger = Logger("test_log", "test_data_correctness")
        self.data_parser = DataParser(self.logger)
        with open("./precision_configs.json", "r") as file:
            self.precisions = json.load(file)


    def test_compare_binary_and_ordinal(self):
        symbol = self.test_job_config["symbol"]
        binary_path = self.test_job_config["binary_path"]
        original_path = self.test_job_config["original_path"]

        self.compare_binary_and_original(symbol, binary_path, original_path)


    def compare_binary_and_original(self, symbol, binary_path, original_path):
        price_precision = self.precisions[symbol][0]
        quote_precision = self.precisions[symbol][1]

        with (open(binary_path, 'rb') as binary_file, open(original_path, 'r') as original_file):
            while True:
                event_time, transaction_time, bids, asks = self.data_parser \
                    .read_binary_record(binary_file, price_precision, quote_precision)
                if event_time is None:
                    self.logger.info("Reach the end of the file.")
                    break

                original_line = original_file.readline()
                original_record = json.loads(original_line)
                original_event_time = original_record["E"]
                original_transaction_time = original_record["T"]
                original_bids = [[float(b[0]), float(b[1])] for b in original_record["b"]]
                original_asks = [[float(a[0]), float(a[1])] for a in original_record["a"]]
                try:
                    self.assertEqual(event_time, original_event_time)
                except AssertionError:
                    self.logger.error("Event time record failed.")
                    self.logger.error(event_time)
                    self.logger.error(original_event_time)

                try:
                    self.assertEqual(transaction_time, original_transaction_time)
                except AssertionError:
                    self.logger.error("Transaction time record failed.")
                    self.logger.error(transaction_time)
                    self.logger.error(original_transaction_time)

                try:
                    self.assertEqual(bids, original_bids)
                except AssertionError as e:
                    self.logger.error("Bids record failed.")
                    self.logger.error(e)

                try:
                    self.assertEqual(asks, original_asks)
                except AssertionError as e:
                    self.logger.error("Asks record failed.")
                    self.logger.error(e)

            self.logger.info("All records comparison successful.")