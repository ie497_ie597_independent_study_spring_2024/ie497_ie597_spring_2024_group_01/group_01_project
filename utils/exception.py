"""
Some exceptions to handle:
1. Ping pong error
2. Rate limit
3. Single connection only lasts for 24 hours. Need to handle the exception and reconnect.
"""