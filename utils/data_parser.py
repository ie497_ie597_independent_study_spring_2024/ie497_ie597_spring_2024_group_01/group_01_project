import json
import struct


class DataParser:
    def __init__(self, logger):
        self.logger = logger
        with open("precision_configs.json", "r") as f:
            self.precisions = json.load(f)

    # Read one message of binary data
    def read_binary_record(self, file, price_precision, quote_precision):
        # 8 bytes for event_time, 8 bytes for transaction_time, 4 bytes for num_bids
        record_header = file.read(20)
        if not record_header:
            return None, None, None, None

        event_time, transaction_time, num_bids = struct.unpack('qqI', record_header)
        bids = []
        for _ in range(num_bids):
            bid_data = file.read(8)  # 4 bytes for price, 4 bytes for quantity
            if not bid_data:
                return None, None, None, None  # Reached the end of the file unexpectedly
            price, quantity = struct.unpack('iI', bid_data)
            bids.append([round(price/10**(price_precision + 1), price_precision),
                         round(quantity/10**(quote_precision+1), quote_precision)])

        num_asks_data = file.read(4)
        if not num_asks_data:
            return None, None, None, None  # Reached the end of the file unexpectedly

        num_asks = struct.unpack('I', num_asks_data)[0]
        asks = []
        for _ in range(num_asks):
            ask_data = file.read(8)  # 4 bytes for price, 4 bytes for quantity
            if not ask_data:
                return None  # Reached the end of the file unexpectedly
            price, quantity = struct.unpack('iI', ask_data)
            asks.append([round(price/10**(price_precision+1), price_precision),
                         round(quantity/10**(quote_precision+1), quote_precision)])

        return event_time, transaction_time, bids, asks

    def write_json(self, message, write_path):
        data = json.loads(message)
        with open(write_path, "a") as file:
            json.dump(data, file)
            file.write("\n")

    def json_to_binary(self, message):
        data = json.loads(message)

        event_time = data["E"]
        transaction_time = data["T"]
        bids = data["b"]
        asks = data["a"]

        price_precision = self.precisions[data['s']][0]
        quote_precision = self.precisions[data['s']][1]

        binary_bids = b''
        for bid in bids:
            quantity = int(float(bid[1]) * 10**(quote_precision + 1))
            price = int(float(bid[0]) * 10**(price_precision + 1))
            binary_bids += struct.pack('iI', price, quantity)

        binary_asks = b''
        for ask in asks:
            quantity = int(float(ask[1]) * 10**(quote_precision + 1))
            price = int(float(ask[0]) * 10**(price_precision + 1))
            binary_asks += struct.pack('iI', price, quantity)

        binary_record = struct.pack('qqI', event_time, transaction_time, len(bids))
        binary_record += binary_bids
        binary_record += struct.pack("I", len(asks))
        binary_record += binary_asks

        return binary_record

    def write_binary(self, binary_record, write_path: str):
        with open(write_path, "ab") as file:
            file.write(binary_record)
