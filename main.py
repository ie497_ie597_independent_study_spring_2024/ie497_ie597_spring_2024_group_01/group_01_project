import argparse
import json
import multiprocessing
import threading
import time
from multiprocessing import Queue
from utils.data_parser import DataParser
from websocket_client.binance_client import BinanceClient
from utils.logger import Logger

"""
Docs: https://binance-docs.github.io/apidocs/futures/en/#websocket-market-streams
btcusdt@depth@100ms
"""

# Jobs:

# Print out data. Used to test connection.
def demo():
    logger = Logger("ws_log", "binance_depth")
    client = BinanceClient(logger, None, False)
    client.run(["btcusdt@depth@100ms"], "um")


# Run multiple clients to fetch and write data. Aims to compare if data files are the same.
def parallel_write():
    logger1 = Logger("ws_log_client1", "binance_depth")
    client1 = BinanceClient(logger1, True, False, 60, write_path="logs/ws_log_client1/ws1_data")

    logger2 = Logger("ws_log_client2", "binance_depth")
    client2 = BinanceClient(logger2, True, False, 60, write_path="logs/ws_log_client2/ws2_data")

    def run_client1():
        client1.run(["btcusdt@depth@100ms"], "um")

    def run_client2():
        time.sleep(0.05)
        client2.run(["btcusdt@depth@100ms"], "um")

    thread1 = threading.Thread(target=run_client1)
    thread2 = threading.Thread(target=run_client2)

    thread1.start()
    thread2.start()

    thread1.join()
    thread2.join()


# Download data from exchange and write them as binary to local machine. This is a pilot task.
def download_binary():
    with open("job_configs.json", "r") as f:
        job_info = json.load(f)
    logger = Logger("download_log", "book_diff_binary")
    client = BinanceClient(logger, True, True, job_info.get("stop_time", 60),
                           write_path=f"logs/data/{job_info['symbol']}/binary.bin")

    client.run(job_info["streams"], job_info["margin_type"])


# Read binary data and convert it back to human readable information. This is a pilot task.
def read_binary():
    logger = Logger("read_binary_log", "read_binary")
    data_parser = DataParser(logger)
    with open("precision_configs.json", "r") as file:
        precision = json.load(file)
    price_precision = precision[args.symbol][0]
    quote_precision = precision[args.symbol][1]
    with open(f"logs/data/{args.symbol}/binary.bin", "rb") as file:
        data_parser.read_binary_record(file, price_precision, quote_precision)


def write_process(message_queue: Queue, write_path: str, logger: Logger, symbol: str, keep_original: bool = False):
    logger.info("Start write process.")
    data_parser = DataParser(logger)
    while True:
        message = message_queue.get()
        if message is None:
            logger.error("Message queue is empty.")
            continue
        elif message == "stop":
            logger.info("Stop writing data.")
            break
        logger.info("Received the message from queue.")
        binary = data_parser.json_to_binary(message)
        data_parser.write_binary(binary, write_path + f"/{symbol}_binary.bin")
        if keep_original:
            data_parser.write_json(message, write_path + f"/{symbol}")


def download_binary_parallel(job_config):
    logger = Logger("writer", str(int(time.time())))
    message_queue = Queue()
    client = BinanceClient(
        logger,
        message_queue,
        True,
        True,
        job_config.get("stop_time", 60),
        job_config["write_path"]
    )
    client_process = multiprocessing.Process(
        target=client.run,
        args=(job_config["streams"], job_config["margin_type"])
    )
    with open("precision_configs.json", "r") as file:
        precision = json.load(file)
    writer_process = multiprocessing.Process(
        target=write_process,
        args=(message_queue, job_config["write_path"], logger, job_config["symbol"], job_config["keep_original"])
    )
    client_process.start()
    writer_process.start()
    client_process.join()
    writer_process.join()
    logger.info("Finish job: download_binary_parallel.")


def main():
    if args.job == "demo":
        demo()
    elif args.job == "parallel_write":
        parallel_write()
    elif args.job == "download_binary":
        download_binary()
    elif args.job == "read_binary":
        read_binary()
    elif args.job == "download_binary_parallel":
        with open("job_configs.json", "r") as file:
            job_configs = json.load(file)["job_configs"]
        try:
            job_config = [item for item in job_configs if item["job_name"] == "download_binary_parallel"][0]
            download_binary_parallel(job_config)
        except IndexError:
            print("No config matches your job.")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="")
    parser.add_argument("--job")
    args = parser.parse_args()
    main()
