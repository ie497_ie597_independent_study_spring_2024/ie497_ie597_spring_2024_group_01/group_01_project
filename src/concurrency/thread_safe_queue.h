//
// Created by Zhicheng Tang on 5/5/24.
//

#ifndef GROUP_01_PROJECT_THREAD_SAFE_QUEUE_H
#define GROUP_01_PROJECT_THREAD_SAFE_QUEUE_H

#include <queue>
#include <mutex>
#include <condition_variable>
#include <functional>
#include <thread>

using namespace std;

template<typename T>
class ThreadSafeQueue {
public:
    void push(T task) {
        {
            lock_guard<mutex> lock(mutex_);
            queue_.push(std::move(task));
        }
        cv_.notify_one();
    }

    T pop() {
        unique_lock<mutex> lock(mutex_);
        cv_.wait(lock, [this] { return !queue_.empty(); });
        T task = std::move(queue_.front());
        queue_.pop();
        return task;
    }

    bool empty() const {
        lock_guard<mutex> lock(mutex_);
        return queue_.empty();
    }


private:
    queue<T> queue_;
    mutable mutex mutex_;
    condition_variable cv_;
};


#endif //GROUP_01_PROJECT_THREAD_SAFE_QUEUE_H
