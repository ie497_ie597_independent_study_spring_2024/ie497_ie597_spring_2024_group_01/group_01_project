//
// Created by Zhicheng Tang on 5/5/24.
//

#ifndef GROUP_01_PROJECT_WORKERTHREAD_H
#define GROUP_01_PROJECT_WORKERTHREAD_H

#include "thread_safe_queue.h"
#include <fstream>
#include <iostream>

class WorkerThread {
public:
    explicit WorkerThread(int id, ThreadSafeQueue<function<void()>> &task_quque);

    ~WorkerThread();

private:
    void workerLoop();

    int id_;
    ThreadSafeQueue<function<void()>> &task_queue_;
    thread thread_;
};


#endif //GROUP_01_PROJECT_WORKERTHREAD_H
