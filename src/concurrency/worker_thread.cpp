//
// Created by Zhicheng Tang on 5/5/24.
//

#include "worker_thread.h"

WorkerThread::WorkerThread(int id, ThreadSafeQueue<std::function<void()>> &task_queue)
        : id_(id), task_queue_(task_queue), thread_(&WorkerThread::workerLoop, this) {};

WorkerThread::~WorkerThread() {
    if (thread_.joinable()) {
        thread_.join();
    }
}

void WorkerThread::workerLoop() {
    while (true) {
        function<void()> task = task_queue_.pop();
        cout << "Worker " << id_ << " consumed a task." << endl;
        task();
    }
}