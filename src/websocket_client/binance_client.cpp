//
// Created by Zhicheng Tang on 3/30/24.
//

#include "binance_client.h"
#include <thread>


BinanceClient::BinanceClient(shared_ptr<Logger> logger, bool write_data, bool write_binary,
                             const chrono::seconds stop_time, const string &write_path, const string &file_name,
                             bool save_original_json)
    : spot_host("stream.binance.com:9443"),
      um_host("fstream.binance.com"),
      logger(logger),
      write_data(write_data),
      write_binary(write_binary),
      write_path(write_path),
      file_name(file_name),
      stop_time(stop_time),
      data_parser(make_unique<DataParser>(logger)) {
    // Configure SSL context
    configure_context(ctx);
    int num_worker_threads = 2;
    for (int i = 0; i < num_worker_threads; ++i) {
        cout << "Add worker " << i << endl;
        worker_threads_.emplace_back(make_unique<WorkerThread>(i, task_queue_));
    }

    // Log the start of the client
    logger->log(INFO, "Binance websocket client started.");
    start_time = chrono::steady_clock::now();
}

void BinanceClient::run(const vector<string> &streams, const string &margin_type) {
    path = getWsUrl(streams);
    if (margin_type == "um") {
        resolver.async_resolve(um_host, "443", [this](auto ec, auto results) {
            onResolve(ec, results);
        });
    } else if (margin_type == "spot" || margin_type == "cm") {
        logger->log(INFO, "Spot streams are not support currently.");
    } else {
        logger->log(ERROR, "Invalid margin type.");
    }

    // Start the IO context in its own thread
    std::thread ioc_thread([this]() {
        ioc.run();
    });
    ioc_thread.join();
}

void BinanceClient::onResolve(beast::error_code ec, tcp::resolver::results_type results) {
    if (ec) return onError(ec, "resolve");

    // Make the connection on the IP address we get from a lookup
    get_lowest_layer(ws).async_connect(results.begin(), results.end(),
                                       [this](beast::error_code ec, tcp::resolver::iterator it) {
                                           onConnect(ec);
                                       });
}

void BinanceClient::onConnect(beast::error_code ec) {
    if (ec) return onError(ec, "connect");

    // Perform the SSL handshake
    ws.next_layer().async_handshake(ssl::stream_base::client, [this](auto ec) {
        onSSLHandshake(ec);
    });
}


void BinanceClient::onSSLHandshake(beast::error_code ec) {
    if (ec) return onError(ec, "ssl_handshake");

    // Perform the WebSocket handshake
    ws.async_handshake(um_host, path, [this](auto ec) {
        onWebSocketHandshake(ec);
    });
}

void BinanceClient::onWebSocketHandshake(beast::error_code ec) {
    if (ec) {
        return onError(ec, "websocket_handshake");
    }
    // Handshake succeeded
    logger->log(INFO, "WebSocket handshake successful.");
    onOpen();
}

void BinanceClient::onOpen() {
    // WebSocket connection opened. Ready to send/receive messages.
    logger->log(INFO, "WebSocket connection opened.");

    // Start reading messages
    doRead();
}

void BinanceClient::doRead() {
    // Read a message into our buffer
    ws.async_read(buffer, [this](auto ec, std::size_t bytes_transferred) {
        onRead(ec, bytes_transferred);
    });
}

void BinanceClient::onRead(beast::error_code ec, std::size_t bytes_transferred) {
    if (ec) return onError(ec, "read");

    // Process the message
    auto msg = beast::buffers_to_string(buffer.data());

    task_queue_.push([msg, this]() {
        handleMessage(msg);
    });

    // Clear the buffer
    buffer.consume(buffer.size());

    // Continue reading messages
    doRead();
}

void BinanceClient::onError(beast::error_code ec, const std::string &what) {
    logger->log(ERROR, "Error on " + what + ": " + ec.message());
}

void BinanceClient::handleMessage(const std::string &message) {
    cout << "Received message: " << message << endl;
    json jsonObject = json::parse(message);
    string stream_name = jsonObject["stream"];
    cout << stream_name << endl;
    json dataObject = jsonObject["data"];
    if (write_data) {
        if (write_binary) {
            auto binary_data = data_parser->jsonToBinary(dataObject, stream_name);
            string full_path = write_path + stream_name + "/book_updates.bin";
            data_parser->writeBinary(binary_data, full_path);
        } else {
            logger->log(INFO, "Writing json data has not been implemented.");
        }
    }
}

void BinanceClient::onClose() {
    logger->log(INFO, "WebSocket connection closed.");
    // You can perform cleanup or reconnection attempts here
}

void BinanceClient::configure_context(ssl::context &ctx) {
    try {
        ctx.set_default_verify_paths();
        ctx.set_verify_mode(ssl::verify_peer);
        // Optionally load certificates or set additional verification here
    } catch (std::exception &e) {
        logger->log(ERROR, std::string("Error configuring SSL context: ") + e.what());
    }
}

string BinanceClient::getWsUrl(const vector<string> &streams, const string &margin_type) {
    if (streams.size() == 1) {
        return "/ws/" + streams[0];
    } else {
        string combined_streams;
        for (const auto &stream: streams) {
            combined_streams += stream + "/";
        }
        combined_streams.pop_back();
        return "/stream?streams=" + combined_streams;
    }
}
