//
// Created by Zhicheng Tang on 3/30/24.
//

#ifndef BINANCE_CLIENT_H
#define BINANCE_CLIENT_H

#include "data_parser.h"
#include "logger.h"
#include "worker_thread.h"
#include <chrono>
#include <string>
#include <vector>
#include <nlohmann/json.hpp>

#include <boost/beast/core.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/beast/ssl.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/asio/ip/tcp.hpp>

// Alias for namespaces
namespace beast = boost::beast;
namespace http = beast::http;
namespace websocket = beast::websocket;
namespace net = boost::asio;
namespace ssl = boost::asio::ssl;

// Alias for types
using tcp = boost::asio::ip::tcp;
using json = nlohmann::json;


class BinanceClient {
public:
    BinanceClient(shared_ptr<Logger> logger, bool write_data, bool write_binary = false,
                  chrono::seconds stop_time = chrono::seconds::zero(),
                  const string &write_path = "", const string &file_name = "",
                  bool save_original_json = false);

    void run(const vector<string> &streams, const string &margin_type = "");

private:
    string spot_host;
    string um_host;
    string path;
    shared_ptr<Logger> logger; // use shared_ptr so that other components could use the same logger.
    bool write_data{};
    bool write_binary{};
    string write_path;
    string file_name;
    chrono::seconds stop_time{};
    unique_ptr<DataParser> data_parser;
    chrono::steady_clock::time_point start_time;

    // Concurrency components
    ThreadSafeQueue<function<void()>> task_queue_;
    vector<unique_ptr<WorkerThread>> worker_threads_;

    // Networking components
    net::io_context ioc;
    ssl::context ctx{ssl::context::tlsv12_client};
    tcp::resolver resolver{ioc};
    websocket::stream<beast::ssl_stream<beast::tcp_stream> > ws{ioc, ctx};
    beast::flat_buffer buffer;

    void onResolve(beast::error_code ec, tcp::resolver::results_type results);

    void onConnect(beast::error_code ec);

    void onSSLHandshake(beast::error_code ec);

    void onWebSocketHandshake(beast::error_code ec);

    void doRead();

    void onRead(beast::error_code ec, std::size_t bytes_transferred);

    void onClose();

    void onOpen();

    void onError(beast::error_code ec, const std::string &what);

    void handleMessage(const std::string &message);

    std::string getWsUrl(const std::vector<std::string> &streams, const std::string &margin_type = "");

    void configure_context(ssl::context &ctx); // Utility function to configure SSL context
};


#endif
