//
// Created by Zhicheng Tang on 3/30/24.
//
#ifndef DATA_PARSER_H
#define DATA_PARSER_H
#include "logger.h"
#include "nlohmann/json.hpp"

struct Order {
    double price;
    double quantity;
};

class DataParser {
public:
    DataParser(std::shared_ptr<Logger> logger);

    std::tuple<uint64_t, uint64_t, std::vector<Order>, std::vector<Order> >
    readBinaryRecord(const string &file_path, int price_precision, int quote_precision);

    static double roundToPrecision(double number, int precision);

    vector<uint8_t> jsonToBinary(const nlohmann::json &message, const string &stream);

    void writeBinary(const vector<uint8_t> &binary_data, const string &file_path);

    nlohmann::json getJobConfig(const string &config_file_path, const string &job_name);

    vector<int> getPrecisions(const string &config_file_path, const string &symbol);

private:
    shared_ptr<Logger> logger;
    unordered_map<string, pair<int, int>> precisions;
    template<typename T>
    void appendToBinary(vector<uint8_t> &binaryData, T value) {
        auto ptr = reinterpret_cast<uint8_t *>(&value);
        binaryData.insert(binaryData.end(), ptr, ptr + sizeof(T));
    }

    void processOrder(vector<uint8_t> &binaryData, const nlohmann::json &orders, int price_precision,
                      int quote_precision);
};


#endif
