//
// Created by Zhicheng Tang on 3/30/24.
//

#include "data_parser.h"
#include "binance_client.h"

DataParser::DataParser(std::shared_ptr<Logger> logger)
    : logger(logger) {
    ifstream config_file("precision_configs.json");
    if (!config_file.is_open()) {
        logger->log(ERROR, "Failed to read configs.");
        throw runtime_error("Unable to open config file.");
    }

    json precisionData;
    config_file >> precisionData;
    for (auto it = precisionData.begin(); it != precisionData.end(); ++it) {
        if (it.key() != "Description") {
            pair<int, int> precision_pair = it.value().get<pair<int, int>>();
            precisions[it.key()] = precision_pair;
        }
    }
};

// General "int" size can vary between compilers. Binary data has fixed-width requirements.
// uint64_t and uint32_t are unsign.
tuple<uint64_t, uint64_t, std::vector<Order>, std::vector<Order> > DataParser::readBinaryRecord(
    const string &file_path, int price_precision, int quote_precision) {
    ifstream file(file_path, ios::binary);
    filesystem::path current_path = filesystem::current_path();
    cout << "Current Directory: " << current_path << endl;
    if (!file) {
        throw runtime_error("Failed to open file");
    }

    // Read the header part
    uint64_t eventTime, transactionTime;
    uint32_t numBids, numAsks;
    file.read(reinterpret_cast<char *>(&eventTime), sizeof(eventTime));
    file.read(reinterpret_cast<char *>(&transactionTime), sizeof(transactionTime));
    file.read(reinterpret_cast<char *>(&numBids), sizeof(numBids));

    vector<Order> bids;
    for (uint32_t i = 0; i < numBids; ++i) {
        int32_t price;
        uint32_t quantity;
        file.read(reinterpret_cast<char *>(&price), sizeof(price));
        file.read(reinterpret_cast<char *>(&quantity), sizeof(quantity));
        if (file.fail()) {
            logger->log(ERROR, "Fail to read the bid orders.");
            return {};
        }
        double adjusted_price = static_cast<double>(price) / pow(10.0, price_precision + 1);
        adjusted_price = roundToPrecision(adjusted_price, price_precision);
        double adjusted_quantity = static_cast<double>(quantity) / pow(10.0, quote_precision + 1);
        adjusted_quantity = roundToPrecision(adjusted_quantity, quote_precision);
        bids.push_back({adjusted_price, adjusted_quantity});
    }

    // Read the number of asks
    file.read(reinterpret_cast<char *>(&numAsks), sizeof(numAsks));
    if (file.fail()) {
        logger->log(ERROR, "Fail to read the number of of asks");
        return {};
    }
    std::vector<Order> asks;
    for (uint32_t i = 0; i < numAsks; ++i) {
        int32_t price;
        uint32_t quantity;
        file.read(reinterpret_cast<char *>(&price), sizeof(price));
        file.read(reinterpret_cast<char *>(&quantity), sizeof(price));
        if (file.fail()) {
            return {};
        }
        double adjusted_price = static_cast<double>(price) / pow(10.0, price_precision + 1);
        adjusted_price = roundToPrecision(adjusted_price, price_precision);
        double adjusted_quantity = static_cast<double>(quantity) / pow(10.0, quote_precision + 1);
        adjusted_quantity = roundToPrecision(adjusted_quantity, quote_precision);
        asks.push_back({adjusted_price, adjusted_quantity});
    }

    return {eventTime, transactionTime, bids, asks};
}


double DataParser::roundToPrecision(double number, int precision) {
    double factor = pow(10, precision);
    return round(number * factor) / factor;
}


void DataParser::processOrder(vector<uint8_t> &binaryData, const nlohmann::json &orders, int price_precision,
                              int quote_precision) {
    for (const auto &order: orders) {
        auto price = static_cast<int>(std::stod(order[0].get<std::string>()) * std::pow(10, price_precision + 1));
        auto quantity = static_cast<unsigned int>(std::stod(order[1].get<std::string>()) * std::pow(
                                                      10, quote_precision + 1));

        appendToBinary(binaryData, price);
        appendToBinary(binaryData, quantity);
    }
}


vector<uint8_t> DataParser::jsonToBinary(const json &data, const string &stream) {
    auto event_time = data["E"].get<int64_t>();
    auto transaction_time = data["T"].get<int64_t>();
    auto bids = data["b"];
    auto asks = data["a"];

    int price_precision;
    int quote_precision;
    string symbol;
    istringstream streamstream(stream);
    getline(streamstream, symbol, '@');

    auto it = precisions.find(symbol);
    if (it != precisions.end()) {
        price_precision = it->second.first;
        quote_precision = it->second.second;
    } else {
        logger->log(ERROR, "Precision of symbol " + symbol + " is not found.");
        throw runtime_error("Precision not found.");
    }


    vector<uint8_t> binaryData;
    stringstream ss;

    appendToBinary(binaryData, event_time);
    appendToBinary(binaryData, transaction_time);
    appendToBinary(binaryData, static_cast<uint32_t>(bids.size()));
    processOrder(binaryData, bids, price_precision, quote_precision);
    appendToBinary(binaryData, static_cast<uint32_t>(asks.size()));
    processOrder(binaryData, asks, price_precision, quote_precision);

    return binaryData;
}

void DataParser::writeBinary(const vector<uint8_t> &binary_data, const string &file_path) {
    std::filesystem::path dir_path = std::filesystem::path(file_path).parent_path();
    // Create directories if they don't exist
    try {
        if (!std::filesystem::exists(dir_path)) {
            std::filesystem::create_directories(dir_path);
            logger->log(INFO, "Created directory: " + dir_path.string());
        }
    } catch (const std::filesystem::filesystem_error &e) {
        logger->log(ERROR, "Failed to create directory: " + std::string(e.what()));
        return;
    }

    ofstream file(file_path, ios::binary | ios::app); // Open in binary mode and append mode
    if (!file.is_open()) {
        logger->log(ERROR, "Failed to open file: " + file_path);
    }

    file.write(reinterpret_cast<const char *>(binary_data.data()), binary_data.size());
    if (file.fail()) {
        logger->log(ERROR, "Failed to write binary data to file.");
    } else {
        logger->log(INFO, "Binary data written successfully to " + file_path);
    }
}

nlohmann::json DataParser::getJobConfig(const string &config_file_path, const string &job_name) {
    ifstream config_file(config_file_path);
    if (!config_file.is_open()) {
        logger->log(ERROR, "Failed to read configs.");
        throw runtime_error("Unable to open config file.");
    }

    nlohmann::json config_json;
    config_file >> config_json;

    for (const nlohmann::basic_json<>& job : config_json["job_configs"]) {
        if (job["job_name"] == job_name) {
            return job;
        }
    }

    logger->log(INFO, "Job not found.");
    throw runtime_error("Job not found.");
}

vector<int> DataParser::getPrecisions(const string &config_file_path, const string &symbol) {
    ifstream config_file(config_file_path);
    if (!config_file.is_open()) {
        logger->log(ERROR, "Failed to read configs.");
        throw runtime_error("Unable to open config file.");
    }

    nlohmann::json config_json;
    config_file >> config_json;
    return config_json[symbol].get<vector<int>>();
}

