//
// Created by Zhicheng Tang on 3/30/24.
//

#ifndef LOGGER_H
#define LOGGER_H
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>

using namespace std;

enum LogLevel {
    INFO,
    ERROR
};

class Logger {
public:
    template <typename... Args>
    static void log(LogLevel level, Args&&... args);

private:
    static string getCurrentTimestamp();
    static string getLogFileName();
};

template <typename... Args>
void Logger::log(LogLevel level, Args&&... args) {
    ostringstream oss;
    (oss << ... << std::forward<Args>(args));
    string message = oss.str();

    std::string levelStr;
    switch (level) {
        case INFO:
            levelStr = "INFO";
            break;
        case ERROR:
            levelStr = "ERROR";
            break;
    }

    std::string timestamp = getCurrentTimestamp();
    std::string logEntry = "[" + timestamp + "] [" + levelStr + "] " + message;

    // Print the log entry to the console
    cout << logEntry << std::endl;

    string fileName = getLogFileName();
    ofstream logFile(fileName, ios::app);
    if (logFile.is_open()) {
        logFile << logEntry << endl;
        logFile.close();
    }

}


#endif
