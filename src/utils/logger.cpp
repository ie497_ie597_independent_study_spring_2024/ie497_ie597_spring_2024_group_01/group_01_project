//
// Created by Zhicheng Tang on 3/30/24.
//

#include "logger.h"
#include <chrono>
#include <ctime>
#include <iomanip>
#include <filesystem>

string Logger::getCurrentTimestamp() {
    auto now = chrono::system_clock::now();
    time_t now_c = chrono::system_clock::to_time_t(now);
    tm* now_tm = gmtime(&now_c);

    char buffer[20];
    strftime(buffer, sizeof(buffer), "%Y-%m-%d %H:%M:%S", now_tm);
    return string(buffer);
}

string Logger::getLogFileName() {
    auto now = chrono::system_clock::now();
    time_t now_c = chrono::system_clock::to_time_t(now);
    tm* now_tm = gmtime(&now_c);

    char buffer[20];
    strftime(buffer, sizeof(buffer), "%Y-%m-%d", now_tm);
    string logDate = string(buffer);
    string folderName = "logs";
    string fileName = folderName + "/" + logDate + ".log";

    // Create the log folder if it does not exist
    filesystem::create_directory(folderName);

    return fileName;
}